import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { ReminderInfo } from '../models/reminder-info';
import { environment } from './../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ReminderinfoService {

  private getUrl: string = environment.apiUrl;

  constructor(private _httpClient: HttpClient) { }

  getReminderInfoList(): Observable<ReminderInfo[]> {
    return this._httpClient.get<ReminderInfo[]>(this.getUrl).pipe(
      map( response => response)
    )
  }

  saveReminderInfo(reminderInfo: ReminderInfo): Observable<ReminderInfo> {
    return this._httpClient.post<ReminderInfo>(this.getUrl, reminderInfo);
  }
  getReminderInfo(id: string): Observable<ReminderInfo> {
    return this._httpClient.get<ReminderInfo>(`${this.getUrl}/${id}`).pipe(
      map(response => response)
    )
  }

  deleteReminderInfo(id: number): Observable<any> {
    return this._httpClient.delete(`${this.getUrl}/${id}`, {responseType: 'text'});
  }
}
