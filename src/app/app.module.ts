import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { HashLocationStrategy, LocationStrategy  } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './auth/token.interceptor';

import { AppComponent } from './app.component';
import { RemindrinfoComponent } from './components/remindrinfo/remindrinfo.component';
import { AddReminderInfoComponent } from './components/add-reminder-info/add-reminder-info.component';

const routers: Routes = [
  {path: '', redirectTo: '/remindrinfo', pathMatch: 'full'},
  {path: 'remindrinfo', component: RemindrinfoComponent},
  {path: 'addRemindrInfo', component: AddReminderInfoComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    RemindrinfoComponent,
    AddReminderInfoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routers)
  ],
  providers: [
    {
      provide : LocationStrategy , 
      useClass: HashLocationStrategy,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
  
})
export class AppModule { }
