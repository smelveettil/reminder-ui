export class ReminderInfo {
    id!: number;
    reminderType!: string;
    description!: string;
}
