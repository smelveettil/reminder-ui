import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReminderInfo } from 'src/app/models/reminder-info';
import { ReminderinfoService } from 'src/app/services/reminderinfo.service';

@Component({
  selector: 'app-add-reminder-info',
  templateUrl: './add-reminder-info.component.html',
  styleUrls: ['./add-reminder-info.component.css']
})
export class AddReminderInfoComponent implements OnInit {

  reminderInfo: ReminderInfo = new ReminderInfo();

  constructor(private _reminderService: ReminderinfoService,
              private _router: Router,
              private _activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    const isIdPresent = this._activatedRoute.snapshot.paramMap.has('id');
    if (isIdPresent) {
        const id = this._activatedRoute.snapshot.paramMap.get('id');
        this._reminderService.getReminderInfo("").subscribe(
          data => this.reminderInfo = data 
        )
    }
  }

  saveReminderInfo() {
    this._reminderService.saveReminderInfo(this.reminderInfo).subscribe(
      data => {
        console.log('response', data);
        this._router.navigateByUrl("/remindrinfo");
      }
    )
  }

  deleteExpense(id: number) {
    this._reminderService.deleteReminderInfo(id).subscribe(
      data => {
        console.log('deleted response', data);
        this._router.navigateByUrl('/remindrinfo');
      }
    )
  }

}
