import { Component, OnInit } from '@angular/core';
import { ReminderInfo } from 'src/app/models/reminder-info';
import { ReminderinfoService } from 'src/app/services/reminderinfo.service';

@Component({
  selector: 'app-remindrinfo',
  templateUrl: './remindrinfo.component.html',
  styleUrls: ['./remindrinfo.component.css']
})
export class RemindrinfoComponent implements OnInit {

  private _reminderInfoList: ReminderInfo[] = [];
  public get reminderInfoList(): ReminderInfo[] {
    return this._reminderInfoList;
  }
  public set reminderInfoList(value: ReminderInfo[]) {
    this._reminderInfoList = value;
  }

  constructor(private _reminderService : ReminderinfoService) { }

  ngOnInit(): void {
    this.listReminderInfo();
  }

  listReminderInfo() {
    this._reminderService.getReminderInfoList().subscribe(
      data => this.reminderInfoList = data
    )
  }


}
