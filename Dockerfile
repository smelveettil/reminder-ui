FROM nginx:1.17.1-alpine
COPY dist/reminder-ui /usr/share/nginx/html
EXPOSE 80
